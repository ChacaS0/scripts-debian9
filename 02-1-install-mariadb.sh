#!/bin/bash
#title           :MariaDB install - Beta
#description     :This script performs the installation of MariaDB on a Debian 9 server
#author          :ChacaS0
#source          :gitlab.com/ChacaS0/scripts-debian9/
#date            :2019-01-13
#version         :0.1b
#usage           :Edit the config variables then run the script as root
#license         :MIT
#notes           :Requires an Internet connection and a correct mirror list setup.
#bash_version    :>=4.4.23(1)-release (might work with older versions)
#========================================================================================================
#
# CONFIG VARS
#
# `db_lang` possible values = ["mariadb", "mysql"]
db_lang="mariadb"


#========================================================================================================
#
# SNIPPETS
# Append here ...

# Define a viarable that would hold the password in `PASSWDDB` 
# and the database name in `MAINDB`. Then you can use the following commands:
# mysql -uroot -p${rootpasswd} -e "CREATE DATABASE ${MAINDB} /*\!40100 DEFAULT CHARACTER SET utf8mb4 */;"
# mysql -uroot -p${rootpasswd} -e "CREATE USER ${MAINDB}@localhost IDENTIFIED BY '${PASSWDDB}';"
# mysql -uroot -p${rootpasswd} -e "GRANT ALL PRIVILEGES ON ${MAINDB}.* TO '${MAINDB}'@'localhost';"
# mysql -uroot -p${rootpasswd} -e "FLUSH PRIVILEGES;"

#
# Useful links
# Append here ...
# DigitalOcean       :https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-debian-9

#========================================================================================================
# 
# PROCESSING
#
{
	# System update
	echo "Updating first..."
	apt-get update
	echo "Update: Done."
	echo ""
	# Installation
	echo "Installing ${db_lang}..."
	apt-get install -y ${db_lang}-server
	echo "Installation: Done."

	# Secure installation
	echo "Entering secure installation..."
	mysql_secure_installation
	echo "Secure installation: Done."


} > >(sed "s/^/\e[35m$(date +%F_%H-%M-%S)  | \e[39m/") # timestamp prefix
