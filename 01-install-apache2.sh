#!/bin/bash
#title           :Apache2 install - Beta
#description     :This script performs the installation of Apache2 & PHP on a Debian 9 server
#author          :ChacaS0
#source          :gitlab.com/ChacaS0/scripts-debian9/
#date            :2019-01-12
#version         :0.1b
#usage           :Edit the config variables then run the script as root
#license         :MIT
#notes           :Requires an Internet connection and a correct mirror list setup.
#bash_version    :>=4.4.23(1)-release (might work with older versions)
#=============================================================================================
#
# CONFIG VARS
#

# Change the values of the following variables in order to personalize the installation.
# which apache
apache="apache2"
apache_doc="apache2-doc"
# how to reload, accepted values = ["reload", "restart"]
end_reload="restart"
# `log_level` possible values = ["warn", "debug"] (#TODO - complete)
log_level="warn"
# `php_version` tested values = ["5", "7.0"]
php_version="7.0"
# Ports
ports_on_http=1
ports_on_https=1

# Website configuration
# # Virtualhost
# `create_virtualhost` 1 -> true; 0 -> false
create_virtualhost=1
site_name="example"
domain_name="example.org"
admin_contact="admin@example.org"
source_path="/var/www/example"
# `create_src_if_missing` when set to `1` create the directory if 
# does not exist, else set to `0`
create_src_if_missing=1
# # SSL
enable_ssl=1
# If `enable_ssl` is set to 0, the value of certificate fields do not matter.
certificate_file_path="/etc/apache2/apache.crt"
certificate_key_path="/etc/apache2/apache.key"
certificate_chain_path="/etc/apache2/intermediate-ca-file.crt"
# `should_generate_certificate` 1 -> true; 0 -> false
# `should_generate_certificate` requires `certificate_file_path` and `certificate_key_path`
# `should_generate_certificate` additional configuration could be required, see tag #0201
should_generate_certificate=1
# Default website
disable_default=1


#=============================================================================================
#
# SNIPPETS
# Append here ...
# 
# SelfSigned SSL Certificate (reauires `openssl`) #! REPLACE VALUES !#
# openssl req -new -x509 -newkey rsa:2048 -days 3650 -nodes -sha256 -out /etc/apache2/apache.crt -keyout /etc/apache2/apache.key -subj "/C=RE/ST=Reunion/L=Le Port/O=Formation/OU=Apache/CN=sitessl.mazone.re"
# 
#
# Useful links
# Append here ...
# 
# 101 linux commands    : https://buzut.fr/101-commandes-indispensables-sous-linux/
# Bash colors           : https://misc.flogisoft.com/bash/tip_colors_and_formatting
# OpenSSL examples      : https://geekflare.com/openssl-commands-certificates/
# 
# GOOD PRACTICES
# Permissions BM301     : https://www.digitalocean.com/community/questions/proper-permissions-for-web-server-s-directory
# Permissions BM301     : https://serverfault.com/a/819714
# 
#=============================================================================================
# 
# PROCESSING
#
{
	# Download and install apache2 and its documentation
	echo "Installing Apache..."
	apt-get install ${apache} ${apache_doc}   
	echo "Apache's installation: Done"

	# Install PHP and its Apache modules
	echo "Installing PHP and its Apache modules..."
	{
		apt-get -y  install php${php_version}
		apt-get -y  install php${php_version}-imap
		apt-get -y  install php${php_version}-xmlrpc
		apt-get -y  install php${php_version}-cgi
		apt-get -y  install php${php_version}-cli
		apt-get -y  install php${php_version}-gd
		apt-get -y  install php${php_version}-mcrypt
		apt-get -y  install php${php_version}-mysql
		apt-get -y  install php${php_version}-json
		apt-get -y  install libapache2-mod-php${php_version}
		apt-get -y  install php-pear
		echo "Done."
	} > >(sed "s/^/\e[34m[PHP${php_version}]\e[39m/ ") # prefixed with [PHPX]

	# Additional tools
	echo "Installing additional tools..."
	apt-get -y  install mcrypt
	apt-get -y  install cronolog
	echo "Additional tools: done."

	# Ports
	{
		listen_list=""
		if [ ${ports_on_https} = 1 ] && [ $ports_on_http = 1 ];then
			# HTTP & HTTPS
			read -r -d '' listen_list << EOM
Listen 0.0.0.0:80
Listen 0.0.0.0:443
EOM
		elif [ ${ports_on_https} = 1 ]; then
			# HTTPS
			listen_list = "Listen 0.0.0.0:443"
		elif [ $ports_on_http = 1 ]; then
			# HTTP
			listen_list = "Listen 0.0.0.0:80"
		fi
		# write ports configuration
		echo "Configuring ports..."
cat << EOF > /etc/apache2/ports.conf
# If you just change the port or add more ports here, you will likely also
# have to change the VirtualHost statement in
# /etc/apache2/sites-enabled/000-default.conf 

${listen_list}

<IfModule ssl_module>
   Listen 0.0.0.0:443
</IfModule> 

<IfModule mod_gnutls.c>
   Listen 0.0.0.0:443
</IfModule>
EOF
		echo "Done."
	} > >(sed "s/^/\e[104m[PORTS]\e[39m/ ") # prefixed with [PORTS]

	# VirtualHost
	{
		if [ ${create_virtualhost} = 1];then
			if [ ${enable_ssl} = 1];then
cat << EOF > ${site_name}.conf
<VirtualHost *:80>
   ServerName ${domain_name}
   # Redirect to SSL
   Redirect / https://${domain_name}
</VirtualHost>

<VirtualHost *:443>
   ServerName ${domain_name}
   ServerAdmin ${admin_contact}
   DocumentRoot ${source_path}

   # SSL
   SSLEngine on
   SSLCertificateFile ${certificate_file_path}
   SSLCertificateKeyFile ${certificate_key_path}
   SSLCertificateChainFile ${certificate_chain_path}

   <Directory ${source_path}/>
      # Options +Indexes
      # Options +ExecCGI
      # AddHandler cgi-script .cgi .pl
      AllowOverride None
      Require all granted
   </Directory>

   ErrorLog /var/log/apache2/${site_name}-error.log

   LogLevel ${log_level}
   CustomLog /var/log/apache2/${site_name}-access.log combined
</VirtualHost>
EOF
				a2enmod rewrite
				# Enable SSL
				a2enmod ssl
				echo "SSL enabled."
			else
			# NO SSL
cat << EOF > ${site_name}.conf
<VirtualHost *:80>
   ServerName ${domain_name}
   ServerAdmin ${admin_contact}
   DocumentRoot ${source_path}

   <Directory ${source_path}/>
      # Options +Indexes
      # Options +ExecCGI
      # AddHandler cgi-script .cgi .pl
      AllowOverride None
      Require all granted
   </Directory>

   ErrorLog /var/log/apache2/${site_name}-error.log

   LogLevel ${log_level}
   CustomLog /var/log/apache2/${site_name}-access.log combined
</VirtualHost>
EOF
			fi
		fi

		if[ ${disable_default} = 1]then;
			a2dissite 000-default
			echo "Default website disabled."
		fi

		echo "${site_name} enabled."
		a2ensite ${site_name}

		echo "Creation: Done."
	} > >(sed "s/^/\e[34m[VHost]\e[39m/ ") # prefixed with [VHost]

	# Source directory
	{
		# create if not exist ?
		if [ ${create_src_if_missing} = 1 ];then
			echo "${source_path} is missing"
			if [ ! -d "$DIRECTORY" ]; then
				mkdir ${source_path}
			fi
			echo ${source_path} created.
		fi

		# Permissions - BM301
		echo "Setting up permissions for directories..."
		find /var/www -type d -exec chmod 775 {} \;  # Change permissions of directories to rwxrwxr-x
		echo "Done."
		echo "Setting up permissions for files..."
        find /var/www -type f -exec chmod 664 {} \;  # Change file permissions to rw-rw-r--
        echo "Done."
		
	} > >(sed "s/^/\e[93m[SRC]\e[39m/ ") # prefixed with [SRC]

	# SSL certificate
	{
		if [ ${should_generate_certificate} = 1]; then
			openssl req -new -x509 -newkey rsa:2048 \
			-days 3650 -nodes -sha256 \
			-out ${certificate_file_path} \
			-keyout ${certificate_key_path} \
			-subj "/C=RE/ST=Reunion/L=Le Port/O=Formation/OU=Apache/CN=${domain_name}" #0201
		fi
	} > >(sed "s/^/\e[96m[SSL]\e[39m/ ") # prefixed with [SSL]

	echo "Reloading Apache..."
	echo "Reload type: ${end_reload}"
	systemctl ${end_reload}
	echo "Apache reloaded."

} > >(sed "s/^/\e[35m$(date +%F_%H-%M-%S)  | \e[39m/") # timestamp prefix


